import request from '@/utils/request'

export async function getRefresherResource (params = []) {
  const res = await request({
    url: `/refresher`,
    method: 'get',
    params,
  })

  return res.data
}