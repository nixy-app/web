import request from '@/utils/request'

export async function createUserPost (data) {
  const res = await request({
    url: '/posts',
    method: 'post',
    data,
  })

  return res.data
}

export async function getPost (id, params) {
  const res = await request({
    url: `/posts/${id}`,
    method: 'get',
    params,
  })

  return res.data
}

export async function deletePost (id) {
  await request({
    url: `/posts/${id}`,
    method: 'delete',
  })
}