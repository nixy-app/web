import request from '@/utils/request'

export async function getUserFriendship(userId, friendId) {
  const res = await request({
    url: `/friendship/${friendId}`,
    method: 'get',
  })

  return res.data
}

export async function requestFriendship (friendId) {
  const res = await request({
    url: `/friendship/${friendId}`,
    method: 'post',
  })

  return res.data
}

export async function destroyFriendship (friendId) {
  const res = await request({
    url: `/friendship/${friendId}`,
    method: 'delete',
  })

  return res.data
}

export async function getFriendshipRequests (params = null) {
  const res = await request({
    url: `/users/me/friendships`,
    method: 'get',
    params,
  })

  return res.data
}

export async function getFriendshipRequestsSent (params = null) {
  const res = await request({
    url: `/users/me/friendships/sent`,
    method: 'get',
    params,
  })

  return res.data
}