import request from '@/utils/request'

export function getUserApi(userId) {
  return request({
    url: `/users/${userId}`,
    method: 'get',
  })
}

export async function getUserProfile(slug) {
  const res = await request({
    url: `/users/${slug}/user-profile`,
    method: 'get'
  })

  return res.data
}

export async function getUserTimeline(params = {}) {
  const res = await request({
    url: `/users/me/timeline`,
    method: 'get',
    params,
  })

  return res.data
}

export async function getUserFriends(userId, params) {
  const res = await request({
    url: `/users/${userId}/friends`,
    method: 'get',
    params,
  })

  return res.data
}

export async function updateUser (data) {
  const formData = new FormData();

  for (let i in data) {
    formData.append(i, data[i])
  }

  formData.append('_method', 'put')
  
  const res = await request({
    url: `/users/me`,
    method: 'post',
    data: formData
  })

  return res.data
}

export async function searchUser(q, params = {}) {
  const res = await request({
    url: `/users/search`,
    method: 'get',
    params: { q, ...params }
  })

  return res.data
}
