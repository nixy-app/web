import request from '@/utils/request'

export async function getPostComments (postId, params) {
  const res = await request({
    url: `/posts/${postId}/comments`,
    method: 'get',
    params,
  })

  return res.data
}

export async function createPostComment (postId, body) {
  const res = await request({
    url: `/posts/${postId}/comments`,
    method: 'post',
    data: { body }
  })

  return res.data
}

export async function likeComment (commentId) {
  const res = await request({
    url: `/comments/${commentId}/like`,
    method: 'post'
  })

  return res.data
}

export async function unlikeComment(commentId) {
  const res = await request({
    url: `/comments/${commentId}/like`,
    method: 'delete'
  })

  return res.data
}