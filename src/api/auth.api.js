import request from '@/utils/request'

export function loginApi ({ token, provider }) {
  return request({
    url: '/auth',
    method: 'post',
    data: { token, provider }
  })
}