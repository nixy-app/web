import request from '@/utils/request'

export async function storeReport(data) {
  await request({
    url: `/reports`,
    method: 'POST',
    data,
  })
}

export async function reportPost (id) {
  await storeReport({
    id,
    type: 'post',
  })
}

export async function reportComment(id) {
  await storeReport({
    id,
    type: 'comment',
  })
}