import request from '@/utils/request'

export async function getNotificationCount() {
  const res = await request({
    url: `/notifications/count`,
    method: 'get',
  })

  return res.data
}

export async function getNotifications (params) {
  const res = await request({
    url: `/notifications`,
    method: 'get',
    params,
  })

  return res.data
}