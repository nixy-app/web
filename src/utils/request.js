import axios from 'axios'
import store from './../store'
import router from './../routes'

const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 30000
})

service.interceptors.request.use(config => {
  const token = localStorage.getItem('token')

  if (token ) {
    config.headers['Authorization'] = 'Bearer ' + token
  }

  return config
})

service.interceptors.response.use(response => {
  return response
}, error => {
  if (error.response.status === 401) {
    store.dispatch('user/logout')
    router.go()
  }

  return Promise.reject(error);
})

export default service