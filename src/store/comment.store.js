import { createPostComment } from '@/api/comment.api'

export default {
  namespaced: true,

  state: () => ({
  }),

  mutations: {
  },

  getters: {
  },

  actions: {
    async createPostComment ({ commit }, { id, body }) {
      commit('post/setPostAsCommented', id, { root: true })
      commit('post/increasePostTimelineCommentCount', id, { root: true })
      return await createPostComment(id, body)
    }
  }
}
