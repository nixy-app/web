import { createStore } from 'vuex'
import user from './user.store'
import post from './post.store'
import friendship from './friendship.store'
import notification from './notification.store'
import comment from './comment.store'
import refresher from './refresher.store'

const store = createStore({
  modules: {
    user,
    post,
    friendship,
    notification,
    comment,
    refresher,
  }
})

export default store