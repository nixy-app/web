export default {
  namespaced: true,

  state: () => ({
  }),

  mutations: {
  },

  getters: {
  },

  actions: {
    friendshipStarted ({ commit }, friend) {
      commit('user/increaseUserFriendCount', null, { root: true })
      commit('notification/decreaseFriendRequestCount', null, { root: true })
    }
  }
}