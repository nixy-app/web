import {
  getUserTimeline,
} from '@/api/user.api'

import {
  createUserPost,
  getPost,
  deletePost,
} from '@/api/post.api'

export default {
  namespaced: true,

  state: () => ({
    userTimeline: [],
    timelineNewestPosts: [],
    timelineNewestPostsTimer: null,
    userTimelineCursor: null,
    isUseTimelineLastPage: false,
    post: {},
  }),

  mutations: {
    resetTimeline (state) {
      state.userTimelineCursor = null
      state.isUseTimelineLastPage = false
      state.userTimeline = []
    },

    addUserTimelinePost (state, { post, position}) {
      if (position === 'start') {
        state.userTimeline.unshift(post)
      } else {
        state.userTimeline.push(post)
      }
    },

    setPost (state, post) {
      state.post = post
    },

    setPostAsCommented (state, postId = null) {
      state.post.has_my_comment = true

      if (postId) {
        const index = state.userTimeline.findIndex(post => post.id == postId)

        if (index !== -1) {
          state.userTimeline[index].has_my_comment = true
        }
      }
    },

    removePostTimeline (state, postId) {
      const index = state.userTimeline.findIndex(post => post.id == postId)

      if (index !== -1) {
        state.userTimeline.splice(index, 1)
      }
    },

    increasePostTimelineCommentCount (state, postId) {
      const index = state.userTimeline.findIndex(post => post.id == postId)

      if (index !== -1) {
        state.userTimeline[index].comment_count++
      }
    },

    updatePostTimeline (state, { id, ...data }) {
      const index = state.userTimeline.findIndex(post => post.id == id)

      if (index !== -1) {
        Object.assign(state.userTimeline[index], data)
      }
    },

    setPostTimelineCommentCount (state, { id, count }) {
      const index = state.userTimeline.findIndex(post => post.id == id)

      if (index !== -1) {
        state.userTimeline[index].comment_count = count
      }
    }
  },

  getters: {
    userTimeline: ({ userTimeline }) => userTimeline,

    timelineLastPostId: ({ userTimeline, timelineNewestPosts }) => timelineNewestPosts[0]?.id || userTimeline[0]?.id,

    timelineNewestPosts: ({ timelineNewestPosts}) => timelineNewestPosts,

    isUserTimelineLastPage: ({ isUserTimelineLastPage}) => isUserTimelineLastPage,

    post: ({ post }) => post
  },

  actions: {
    loadTimelineNewestPosts ({ state, commit }) {
      state.userTimeline = [...state.timelineNewestPosts, ...state.userTimeline]
      state.timelineNewestPosts = []
    },

    async getTimelineNewestPosts ({ state, getters }) {
      const res = await getUserTimeline({ last_id: getters.timelineLastPostId })
      state.timelineNewestPosts = [...res.data, ...state.timelineNewestPosts]
    },

    async getUserTimeline ({ state, commit }) {
      const res = await getUserTimeline({ cursor: state.userTimelineCursor })

      state.userTimelineCursor = res.meta.cursor

      state.isUserTimelineLastPage = !res.meta.cursor

      res.data.forEach(post => commit('addUserTimelinePost', { post }))
    },

    async createUserPost ({ commit }, data) {
      const post = await createUserPost(data)

      commit('addUserTimelinePost', {
        position: 'start',
        post
      })

      commit('user/increasePostCount', null, { root: true })
    },

    async deletePost ({ commit }, id) {
      await deletePost(id)
      commit('removePostTimeline', id)
    },

    async getPost ({ commit }, id) {
      const post = await getPost(id)
      commit('setPost', post)
    }
  }
}