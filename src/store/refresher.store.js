import {
  getRefresherResource
} from '@/api/refresher.api'

export default {
  namespaced: true,

  state: () => ({
    refresherTimer: null,
  }),

  mutations: {
  },

  getters: {
  },

  actions: {
    async getRefresherResource ({ commit, rootGetters }) {
      const data = await getRefresherResource({
        post_id: rootGetters['post/userTimeline'].map(_ => _.id).join(',')
      })

      commit('notification/setNotificationCount', data.notification.notification_count, { root: true })
      commit('notification/setFriendRequestCount', data.notification.friend_request_count, { root: true })
      commit('user/updateUser', data.user_stats, { root: true })

      data.posts_stats.forEach(post => {
        commit('post/updatePostTimeline', post, { root: true })
      })
    },

    async initRefresherTimer ({ dispatch, state }) {
      state.refresherTimer = setInterval(() => dispatch('getRefresherResource'), 30000)
      dispatch('getRefresherResource')
    },

    clearRefresherTimer ({ state }) {
      clearInterval(state.refresherTimer)
      state.refresherTimer = null
    }
  }
}