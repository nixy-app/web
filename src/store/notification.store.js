import {
  getNotificationCount
} from '@/api/notification.api'

export default {
  namespaced: true,

  state: () => ({
    friendRequestCount: 0,
    notificationCount: 0,
    notificationCountTimer: null,
  }),

  mutations: {
    decreaseFriendRequestCount: (state) => state.friendRequestCount ? state.friendRequestCount-- : state.friendRequestCount,

    resetNotificationCount: (state) => state.notificationCount = 0,

    setNotificationCount: (state, count) => state.notificationCount = count,

    setFriendRequestCount: (state, count) => state.friendRequestCount = count,
  },

  getters: {
    friendRequestCount: ({ friendRequestCount}) => friendRequestCount,

    notificationCount: ({ notificationCount}) => notificationCount,
  },

  actions: {
    async getNotificationCount ({ state }) {
      const counters = await getNotificationCount()
      state.friendRequestCount = counters.friend_request_count
      state.notificationCount = counters.notification_count
    }
  }
}