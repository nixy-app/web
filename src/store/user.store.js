import { deleteAuthToken } from '@/services/auth.service'

import router from '@/routes'

import {
  getUserApi,
  updateUser,
  getUserProfile,
} from '@/api/user.api'

export default {
  namespaced: true,

  state: () => ({
    user: {},
    hasRequestedLogout: false,
    userProfile: {},
  }),

  mutations: {
    resetUser: (state) => state.user = {},

    increaseUserFriendCount: (state) => state.user.friend_count++,
  
    decreaseUserFriendCount: (state) => state.user.friend_count--,

    increasePostCount: (state) => state.user.post_count++,

    updateUser: (state, data) => Object.assign(state.user, data),

    updateUserProfile: (state, data) => {
      const { name, slug, picture } = data
      
      state.userProfile = Object.assign({}, state.userProfile, { name, slug, picture })
    }
  },

  getters: {
    isLogged: ({ user, hasRequestedLogout }) => Boolean(Object.keys(user).length) && !hasRequestedLogout,

    user: ({ user }) => user,

    userProfile: ({ userProfile }) => userProfile,
  },

  actions: {
    async fetchUser ({ state, dispatch }) {
      const res = await getUserApi('me')

      Object.assign(state.user, res.data)

      await dispatch('refresher/initRefresherTimer', null, { root: true })
    },

    async updateUser ({ state, commit }, data) {
      const user = await updateUser(data)

      state.user = user

      if (state.userProfile.id === user.id) {
        commit('updateUserProfile', user)
      }
    },

    async getUserProfile ({ state }, slug) {
      state.userProfile = await getUserProfile(slug)
    },

    async logout ({ commit, state, dispatch, rootState }) {
      state.hasRequestedLogout = true

      await router.replace({ name: 'login' })

      deleteAuthToken()

      commit('resetUser')

      state.hasRequestedLogout = false

      commit('post/resetTimeline', null, { root: true })
      dispatch('refresher/clearRefresherTimer', null, { root: true })
      clearInterval(rootState.post.timelineNewestPostsTimer)
    }
  }
}