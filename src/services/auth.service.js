export function setAuthToken (token) {
  localStorage.setItem('token', token)
}

export function getAuthToken () {
  return localStorage.getItem('token')
}

export function deleteAuthToken () {
  localStorage.removeItem('token')
}

export const facebookProvider = {
  loadScript() {
    return new Promise((resolve) => {
      window.fbAsyncInit = function () {
        window.FB.init({
          appId: '229268748472330',
          autoLogAppEvents: true,
          xfbml: true,
          version: 'v12.0'
        });
      };

      const el = document.createElement('script')
      el.src = 'https://connect.facebook.net/pt_BR/sdk.js'
      el.onload = () => resolve()

      document.head.appendChild(el)
    })
  },

  signIn () {
    return new Promise(resolve => {
      window.FB.login(function (response) {
        const token = response.authResponse.accessToken
        
        resolve(token)
      }, { scope: 'public_profile,email' });
    })
  }
}

export const googleProvider = {
  loadScript () {
    return new Promise((resolve) => {
      const el = document.createElement('script')

      el.src = 'https://apis.google.com/js/api.js'

      el.onload = () => {
        window.gapi.load('client:auth2', async () => {
          try {
            await window.gapi.client.init({
              apiKey: process.env.VUE_APP_GOOGLE_API_KEY,
              clientId: process.env.VUE_APP_GOOGLE_CLIENT_ID,
              scope: 'https://www.googleapis.com/auth/userinfo.email',
            })
          } catch (err) {
            console.error(err)
          }

          resolve()
        })
      }

      document.head.appendChild(el)
    })
  },

  async signIn () {
    await window.gapi.auth2.getAuthInstance().signIn()
    return window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token
  },
}
