import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core"
import {
  faCamera,
  faHouseUser,
  faUsers,
  faUser,
  faUserSecret,
  faBell,
  faMask,
  faHeart,
  faPowerOff,
  faCog,
  faComment,
  faComments,
  faSearch,
  faPencilAlt,
  faExclamationTriangle,
  faEllipsisH,
  faTimes,
} from "@fortawesome/free-solid-svg-icons"

import {
  faWhatsapp,
  faFacebook,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons'


library.add(faCamera)
library.add(faHouseUser)
library.add(faUsers)
library.add(faUser)
library.add(faTimes)
library.add(faUserSecret)
library.add(faBell)
library.add(faMask)
library.add(faHeart)
library.add(faPowerOff)
library.add(faCog)
library.add(faComment)
library.add(faComments)
library.add(faSearch)
library.add(faPencilAlt)
library.add(faHouseUser)
library.add(faExclamationTriangle)
library.add(faWhatsapp)
library.add(faFacebook)
library.add(faTwitter)
library.add(faEllipsisH)

export default FontAwesomeIcon