import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import routes from './routes'
import { getAuthToken } from '@/services/auth.service'
import vfmPlugin from 'vue-final-modal'
import Toast, { POSITION } from 'vue-toastification'
import FontAwesomeIcon from './icons'
import ConfirmationMixin from '@/mixins/global/ConfirmationMixin'

import 'bootstrap'
import 'vue-toastification/dist/index.css'
import './registerServiceWorker'

const init = async () => {
  if (getAuthToken()) {
    await store.dispatch('user/fetchUser')
  }
}

init().then(() => {
  const app = createApp(App)

  app.component('font-awesome-icon', FontAwesomeIcon)

  app.mixin(ConfirmationMixin)

  app.use(store)
  app.use(routes)
  app.use(vfmPlugin)
  app.use(Toast, {
    position: POSITION.BOTTOM_CENTER,
    timeout: 8000,
    hideProgressBar: true
  })

  app.mount('#app')
})
