import { createRouter, createWebHistory } from 'vue-router'
import guestGuard from './guards/guest.guard'
import authGuard from './guards/auth.guard'

const routes = [
  {
    path: '/',
    component: () => import('@/views/layouts/MasterLayout'),
    children: [
      {
        path: '/',
        redirect: '/home'
      }, {
        path: 'home',
        name: 'home',
        component: () => import('@/views/home/HomePage'),
        meta: {
          guards: [authGuard],
        }
      }, {
        path: '/@:slug',
        name: 'user.profile',
        component: () => import('@/views/user/UserProfilePage'),
        meta: {
          guards: [authGuard]
        }
      }, {
        path: '/post/:id',
        name: 'post',
        component: () => import('@/views/post/PostPage'),
        meta: {
          guards: [authGuard]
        }
      },

      // Search
      {
        path: 'search',
        name: 'search',
        component: () => import('@/views/search/SearchPage')
      },

      // User
      {
        path: 'notifications',
        name: 'notifications',
        component: () => import('@/views/user/UserNotificationsPage')
      },
      {
        path: 'friends',
        name: 'friends',
        component: () => import('@/views/user/UserFriendsPage')
      },
      {
        path: 'friends/requests',
        name: 'friends.requests',
        component: () => import('@/views/user/UserFriendsRequestsPage')
      },
      {
        path: 'friends/requests/sent',
        name: 'friends.requests.sent',
        component: () => import('@/views/user/UserFriendsRequestsSentPage')
      },
      {
        path: 'friends/invite',
        name: 'friends.invite',
        component: () => import('@/views/user/UserFriendsInvitePage')
      }
    ]
  }, {
    path: '/login',
    name: 'login',
    component: () => import('@/views/auth/LoginPage'),
    meta: {
      guards: [guestGuard]
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return { ...savedPosition, behavior: 'instant' }
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  const guards = to.meta?.guards
  // const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)

  // if (nearestWithTitle) {
  //   document.title = nearestWithTitle.meta.title + ' | Nixy'
  // } else {
  //   document.title = 'Nixy'
  // }

  if (guards?.length) {
    for (let i in guards) {
      if (guards[i].fails({ to, from, next })) {
        return next(guards[i].redirect)
      }
    }
  }

  next()
})

export default router
