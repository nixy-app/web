import store from "../../store"

export default {
  fails: () => !store.getters['user/isLogged'],
  redirect: '/login'
}