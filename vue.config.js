module.exports = {
  pwa: {
    workboxOptions: {
      skipWaiting: true,
      clientsClaim: true,
    }
  },
  devServer: {
    disableHostCheck: true,
    https: true,
    port: 8080,
  },

  css: {
    loaderOptions:{
      sass: {
        additionalData: `
          @import '@/assets/scss/variables';
        `
      }
    }
  },
}